package by.ralovets.ralovets

enum class SettingsValues(val setting: String) {

    APP_PREFERENCES("app_pref"),
    MODE_COMPACT("compact"),
    THEME_DARK("dark"),
    IS_LAUNCHER_VISITED("visited");
}