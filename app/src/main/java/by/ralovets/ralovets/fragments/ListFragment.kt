package by.ralovets.ralovets.fragments

import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import by.ralovets.ralovets.data.IconListAdapter
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.ralovets.ralovets.data.AppDetails

import by.ralovets.ralovets.R
import by.ralovets.ralovets.activities.MyApplication
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment(), IconListAdapter.OnLongClickListListener, IconListAdapter.OnClickListListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var myView: View? = null
    private var apps = mutableListOf<AppDetails>()
    private lateinit var packageManager: PackageManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        myView = inflater.inflate(R.layout.fragment_list, container, false)
        return myView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        packageManager = activity!!.packageManager
        var i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)

        var availableActivities = packageManager.queryIntentActivities(i, 0)
        for (resolveInfo in availableActivities) {
            apps.add(AppDetails(
                    resolveInfo.loadLabel(packageManager),
                    resolveInfo.activityInfo.packageName,
                    resolveInfo.activityInfo.loadIcon(packageManager)
            ))
        }

        viewManager = LinearLayoutManager(activity?.applicationContext)
        viewAdapter = IconListAdapter(apps, activity?.applicationContext as MyApplication, this, this)

        recyclerView = items_list.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }

        fab_list.setOnClickListener {

        }
    }

    override fun onLongListItemClick(position: Int) {
//        Toast.makeText(activity?.applicationContext, "#" + ((activity?.applicationContext as MyApplication).myDataSet[position] and 0xFFFFFF).toString(16).toUpperCase(), Toast.LENGTH_SHORT).show()

        myView?.let {
            Snackbar.make(it, "Удалить " + apps[position].label + "?", Snackbar.LENGTH_LONG)
                .setAction("Удалить") { Log.i("Snackbar", "Удалил") }
                .addCallback(object : Snackbar.Callback() {
                    override fun onShown(sb: Snackbar?) {
                        Log.i("Snackbar", "Показан")
                    }

                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        when(event) {
                            DISMISS_EVENT_TIMEOUT -> Log.i("Snackbar", "Закрыт по таймауту")
                            DISMISS_EVENT_ACTION -> {
                                Log.i("Snackbar", "Закрыт по экшену")

//                                (activity?.applicationContext as MyApplication).myDataSet.removeAt(position)
//
//                                recyclerView.adapter?.notifyItemRemoved(position)
                            }
                            DISMISS_EVENT_MANUAL -> Log.i("Snackbar", "Закрыт вручную")
                            DISMISS_EVENT_SWIPE -> Log.i("Snackbar", "Закрыт свайпом")
                        }
                    }
                })
                .show()
        }

        Log.d(TAG, "onClick: looooong clicked");
    }

    override fun onListItemClick(position: Int) {
        var i: Intent? = packageManager.getLaunchIntentForPackage(apps[position].name.toString())
        startActivity(i)
    }


}
