package by.ralovets.ralovets.fragments.welcome

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import by.ralovets.ralovets.activities.MyApplication
import by.ralovets.ralovets.R
import by.ralovets.ralovets.SettingsValues
import kotlinx.android.synthetic.main.fragment_layout.*

class LayoutFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val sharedPref = activity?.getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        sharedPref?.edit()?.apply()
        val defValue = resources.getBoolean(R.bool.default_layout_value)
        val isCompactLayout = sharedPref?.getBoolean(SettingsValues.MODE_COMPACT.setting, defValue)
        val editor = sharedPref?.edit()

        if ((isCompactLayout != null) && (isCompactLayout)) {
            checkButtonDense()
//            containerButtonDense.setBackgroundResource(R.drawable.radiobutton_checked)
//            containerButtonStandart.setBackgroundResource(R.drawable.radiobutton_unchecked)
        } else {
            checkButtonStandart()
//            containerButtonDense.setBackgroundResource(R.drawable.radiobutton_unchecked)
//            containerButtonStandart.setBackgroundResource(R.drawable.radiobutton_checked)
        }

        buttonStandart.setOnClickListener {

            if (editor != null) {
                editor.putBoolean(SettingsValues.MODE_COMPACT.setting, false)
                editor.apply()
            }

            onButtonStandartAreaClick()
        }

        buttonDense.setOnClickListener {

            if (editor != null) {
                editor.putBoolean(SettingsValues.MODE_COMPACT.setting, true)
                editor.apply()
            }

            onButtonDenseAreaClick()
        }

        containerButtonDense.setOnClickListener {

            if (editor != null) {
                editor.putBoolean(SettingsValues.MODE_COMPACT.setting, true)
                editor.apply()
            }

            onButtonDenseAreaClick()
        }

        containerButtonStandart.setOnClickListener {

            if (editor != null) {
                editor.putBoolean(SettingsValues.MODE_COMPACT.setting, false)
                editor.apply()
            }

            onButtonStandartAreaClick()
        }
    }

    private fun onButtonStandartAreaClick() {
        (activity?.applicationContext as MyApplication).isDenseLayout = false
        checkButtonStandart()

//        containerButtonDense.setBackgroundResource(R.drawable.radiobutton_unchecked)
//        containerButtonStandart.setBackgroundResource(R.drawable.radiobutton_checked)
    }

    private fun onButtonDenseAreaClick() {
        (this.activity?.applicationContext as MyApplication).isDenseLayout = true
        checkButtonDense()

//        containerButtonDense.setBackgroundResource(R.drawable.radiobutton_checked)
//        containerButtonStandart.setBackgroundResource(R.drawable.radiobutton_unchecked)
    }

    private fun checkButtonDense() {
        buttonDense.isChecked = true
        buttonStandart.isChecked = false
    }

    private fun checkButtonStandart() {
        buttonDense.isChecked = false
        buttonStandart.isChecked = true
    }
}
