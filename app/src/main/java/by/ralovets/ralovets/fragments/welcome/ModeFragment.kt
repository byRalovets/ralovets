package by.ralovets.ralovets.fragments.welcome

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import by.ralovets.ralovets.R
import by.ralovets.ralovets.SettingsValues
import by.ralovets.ralovets.activities.WelcomePageActivity
import kotlinx.android.synthetic.main.fragment_mode.*


class ModeFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_mode, container, false)
    }

    @SuppressLint("CommitPrefEdits")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val sharedPref = activity?.getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        sharedPref?.edit()?.apply()
        val editor = sharedPref?.edit()

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            checkButtonDark()
//            containerButtonDark.setBackgroundResource(R.drawable.radiobutton_checked)
//            containerButtonLight.setBackgroundResource(R.drawable.radiobutton_unchecked)
        } else {
            checkButtonLight()
//            containerButtonDark.setBackgroundResource(R.drawable.radiobutton_unchecked)
//            containerButtonLight.setBackgroundResource(R.drawable.radiobutton_checked)
        }

        buttonLight.setOnClickListener{

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            buttonDark.isChecked = false

            if (editor != null) {
                editor.putBoolean(SettingsValues.THEME_DARK.setting, false)
                editor.apply()
            }
        }

        buttonDark.setOnClickListener{

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            buttonLight.isChecked = false

            if (editor != null) {
                editor.putBoolean(SettingsValues.THEME_DARK.setting, true)
                editor.apply()
            }
        }

        containerButtonLight.setOnClickListener {

            if (editor != null) {
                editor.putBoolean(SettingsValues.THEME_DARK.setting, false)
                editor.apply()
            }

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            checkButtonLight()
        }

        containerButtonDark.setOnClickListener {

            if (editor != null) {
                editor.putBoolean(SettingsValues.THEME_DARK.setting, true)
                editor.apply()
            }

            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            checkButtonDark()
        }
    }

    fun checkButtonLight() {
        buttonDark.isChecked = false
        buttonLight.isChecked = true
    }

    fun checkButtonDark() {
        buttonDark.isChecked = true
        buttonLight.isChecked = false
    }
}
