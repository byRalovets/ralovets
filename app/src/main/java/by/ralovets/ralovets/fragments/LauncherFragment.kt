package by.ralovets.ralovets.fragments

import by.ralovets.ralovets.data.IconGridAdapter
import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import by.ralovets.ralovets.data.AppDetails
import by.ralovets.ralovets.data.IconDecorator

import by.ralovets.ralovets.R
import by.ralovets.ralovets.SettingsValues
import by.ralovets.ralovets.activities.MyApplication
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_launcher.*
import kotlin.math.roundToInt

class LauncherFragment : Fragment(), IconGridAdapter.OnLongClickGridListener, IconGridAdapter.OnClickGridListener {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private var myView: View? = null
    private var apps = mutableListOf<AppDetails>()
    private lateinit var packageManager: PackageManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        myView = inflater.inflate(R.layout.fragment_launcher, container, false)
        return myView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val columnsCount: Int
        val sharedPref = activity?.getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        sharedPref?.edit()?.apply()
        val defValue = resources.getBoolean(R.bool.default_layout_value)
        val isCompactLayout = sharedPref?.getBoolean(SettingsValues.MODE_COMPACT.setting, defValue)

        columnsCount = if (isCompactLayout != null) {
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
                if (isCompactLayout) 7 else 6
            else
                if (isCompactLayout) 5 else 4
        } else {
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) 4 else 6
        }

        packageManager = activity!!.packageManager
        var i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)

        var availableActivities = packageManager.queryIntentActivities(i, 0)
        for (resolveInfo in availableActivities) {
            apps.add(AppDetails(
                    resolveInfo.loadLabel(packageManager),
                    resolveInfo.activityInfo.packageName,
                    resolveInfo.activityInfo.loadIcon(packageManager)
            ))
        }

        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.icon_offset)

        // TODO: Change to size
        val screenWidth = activity?.windowManager?.defaultDisplay?.width

        val offset = resources.getDimensionPixelSize(R.dimen.icon_offset)
        val sumOffset = 1.5f * (3 * offset + (columnsCount - 1) * offset / 2);
        val elementWidth = ((screenWidth?.minus(sumOffset))?.div(columnsCount))?.roundToInt() ?: 0

        viewManager = GridLayoutManager(activity?.applicationContext, columnsCount)

        val delta = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
            40
        else
            15

        viewAdapter = IconGridAdapter(apps, activity?.applicationContext as MyApplication, this, this,elementWidth - delta )

        recyclerView = icons_grid.apply {
            addItemDecoration(IconDecorator(spacingInPixels));
            layoutManager = viewManager
            adapter = viewAdapter
        }

        fab_grid.setOnClickListener {
            //dfd
        }

    }

    override fun onLongGridItemClick(position: Int) {

        myView?.let {
            Snackbar.make(it, "Удалить " + apps[position].label + "?", Snackbar.LENGTH_LONG)
                    .setAction("Удалить") { Log.i("Snackbar", "Удалил") }
                    .addCallback(object : Snackbar.Callback() {
                        override fun onShown(sb: Snackbar?) {
                            Log.i("Snackbar", "Показан")
                        }

                        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                            when(event) {
                                DISMISS_EVENT_TIMEOUT -> Log.i("Snackbar", "Закрыт по таймауту")
                                DISMISS_EVENT_ACTION -> {
                                    Log.i("Snackbar", "Закрыт по экшену")

                                    (activity?.applicationContext as MyApplication).myDataSet.removeAt(position)

                                    recyclerView.adapter?.notifyItemRemoved(position)
                                }
                                DISMISS_EVENT_MANUAL -> Log.i("Snackbar", "Закрыт вручную")
                                DISMISS_EVENT_SWIPE -> Log.i("Snackbar", "Закрыт свайпом")
                            }
                        }
                    })
                    .show()
        }



        Log.d(TAG, "onClick: looooong clicked");
    }

    override fun onGridItemClick(position: Int) {
        var i: Intent? = packageManager.getLaunchIntentForPackage(apps[position].name.toString())
        startActivity(i)
    }
}