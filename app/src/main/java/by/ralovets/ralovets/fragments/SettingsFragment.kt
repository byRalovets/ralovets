package by.ralovets.ralovets.fragments

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatDelegate

import by.ralovets.ralovets.R
import by.ralovets.ralovets.SettingsValues
import by.ralovets.ralovets.activities.MyApplication
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        switchMode.isChecked = (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)

        val sharedPref = activity?.getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        val prefEditor = sharedPref?.edit()
        prefEditor?.apply()
        val defLayoutValue = resources.getBoolean(R.bool.default_layout_value)

        if (sharedPref != null) {
            switchLayout.isChecked = sharedPref.getBoolean(SettingsValues.MODE_COMPACT.setting, defLayoutValue)
        }

        switchLayout.setOnCheckedChangeListener{ _: CompoundButton, _: Boolean ->
            if (prefEditor != null) {
                prefEditor.putBoolean(SettingsValues.MODE_COMPACT.setting, switchLayout.isChecked)
                prefEditor.apply()
                prefEditor.commit()
            }
        }

        switchMode.setOnCheckedChangeListener { _: CompoundButton, _: Boolean ->
            run {
                prefEditor?.putBoolean(SettingsValues.THEME_DARK.setting, switchMode.isChecked)
                prefEditor?.apply()
                prefEditor?.commit()
                if (switchMode.isChecked)
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                else
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }
    }
}