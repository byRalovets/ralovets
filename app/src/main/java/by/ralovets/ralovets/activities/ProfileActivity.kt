package by.ralovets.ralovets.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import by.ralovets.ralovets.R
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.activity_profile_content.*


class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        phone_block.setOnClickListener {

            if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Permission is not granted
                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                                Manifest.permission.CALL_PHONE)) {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this,
                            arrayOf(Manifest.permission.CALL_PHONE),
                            1)

                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                    // app-defined int constant. The callback method gets the
                    // result of the request.
                }
            } else {
                val uri = "tel:" + getString(R.string.mobile_phone_number)
                val intent = Intent(Intent.ACTION_CALL)
                intent.data = Uri.parse(uri)
                startActivity(intent)
            }
        }

        email_block.setOnClickListener {
            var i = Intent(Intent.ACTION_SEND);
            i.type = "message/rfc822";
            i.putExtra(Intent.EXTRA_EMAIL  , getString(R.string.email_address));
            i.putExtra(Intent.EXTRA_SUBJECT, "Subject");
            i.putExtra(Intent.EXTRA_TEXT   , "Body");
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (e: Exception) {

            }
        }

        address_block.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.google.com/maps/place/проспект+Дзержинского+97,+Минск+220089/@53.8640824,27.4830643,17z/data=!3m1!4b1!4m5!3m4!1s0x46dbd079612bdcb7:0x12dc1f12bc4e078c!8m2!3d53.864079!4d27.485253"))
            startActivity(intent)
        }

        gitlab_block.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://gitlab.com/byRalovets/ralovets"))
            startActivity(intent)
        }

        vk_block.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://vk.com/by.ralovets"))
            startActivity(intent)
        }
    }
}
