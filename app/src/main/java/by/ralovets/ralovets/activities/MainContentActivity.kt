package by.ralovets.ralovets.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import by.ralovets.ralovets.R
import by.ralovets.ralovets.SettingsValues
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.nav_header_navigation_drawer.*

class MainContentActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var mContext: Context

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val defSharedPref = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        if (defSharedPref.getBoolean("NEW_APP", true)) {
            startActivity(Intent(this, WelcomePageActivity::class.java))
        }
        setContentView(R.layout.activity_main_content)
            var sharedPref = getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
            var editor = sharedPref.edit()
            editor.apply()
            editor.putBoolean(SettingsValues.IS_LAUNCHER_VISITED.setting, true)
            editor.apply()
            val mode = if (sharedPref.getBoolean(SettingsValues.THEME_DARK.setting, false))
                AppCompatDelegate.MODE_NIGHT_YES
            else
                AppCompatDelegate.MODE_NIGHT_NO
            AppCompatDelegate.setDefaultNightMode(mode)

        mContext = applicationContext


        val toolbar: Toolbar = findViewById(R.id.main_content_toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawerLayout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)

        appBarConfiguration = AppBarConfiguration(
                setOf(
                        R.id.launcherFragment, R.id.listFragment
                ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.nav_drawer, menu)

        nav_header_image.setOnClickListener{

            val intent = Intent();
            intent.setClass(it.context, ProfileActivity::class.java)
            startActivity(intent)
        }

        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }



}
