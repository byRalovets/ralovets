package by.ralovets.ralovets.activities

import android.app.Application
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatDelegate
import by.ralovets.ralovets.data.IconView
import kotlin.random.Random

class MyApplication : Application() {

    val DEFAULT_LAYOUT_STATE = false

    var isDenseLayout: Boolean = DEFAULT_LAYOUT_STATE
        get() = field
        set(value) {
            field = value
        }

    var isLandOrientation: Boolean = false

    var myDataSet = ArrayList<Int>()
        get() {
            return field
        }

    val random = Random(2452)

    init {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        isDenseLayout = false
        for (a in 1..1000) {
            myDataSet.add((random.nextInt() and 0xFFFFFF) or 0xFF000000.toInt())
        }
    }

    fun getColumnsNumber(): Int {
        return if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE)
            if (isDenseLayout) 7 else 6
        else
            if (isDenseLayout) 5 else 4
    }

    var iconsList = ArrayList<IconView>()
}