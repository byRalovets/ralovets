package by.ralovets.ralovets.activities

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ApplicationInfo
import android.graphics.Bitmap
import android.graphics.Canvas
import android.media.Image
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatDelegate
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import by.ralovets.ralovets.R
import by.ralovets.ralovets.SettingsValues
import by.ralovets.ralovets.data.WelcomeAdapter
import by.ralovets.ralovets.fragments.welcome.DescriptionFragment
import by.ralovets.ralovets.fragments.welcome.GreetingFragment
import by.ralovets.ralovets.fragments.welcome.LayoutFragment
import by.ralovets.ralovets.fragments.welcome.ModeFragment
import kotlinx.android.synthetic.main.activity_welcome_page.*
import kotlinx.android.synthetic.main.fragment_mode.*
import kotlin.math.hypot

class WelcomePageActivity : AppCompatActivity() {

    // Application settings

    // ViewPager adapter
    private lateinit var pagerAdapter: PagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {

        val sharedPref = getSharedPreferences(SettingsValues.APP_PREFERENCES.setting, Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.apply()

        val mode = if (sharedPref.getBoolean(SettingsValues.THEME_DARK.setting, false))
            AppCompatDelegate.MODE_NIGHT_YES
        else
            AppCompatDelegate.MODE_NIGHT_NO
        AppCompatDelegate.setDefaultNightMode(mode)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_page)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            prepareDots(0)

        // Create fragments on welcome page
        val fragments = listOf(
                GreetingFragment(),
                DescriptionFragment(),
                ModeFragment(),
                LayoutFragment()
        )

        pagerAdapter = WelcomeAdapter(supportFragmentManager, fragments)
        pager.adapter = pagerAdapter

        // Add listener no nextButton
        buttonNextFragment.setOnClickListener {
            with(pager) {
                if (currentItem < 3)
                   pager.arrowScroll(View.FOCUS_RIGHT)
                else {

                    startActivity(Intent(applicationContext, MainContentActivity::class.java))

                    val defSharedPref = PreferenceManager.getDefaultSharedPreferences(applicationContext)
                    defSharedPref.edit().apply {
                        putBoolean("NEW_APP", false)
                        apply()
                    }

                    finishAffinity()
                }
            }
        }

        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onPageSelected(position: Int) {
                prepareDots(pager.currentItem)
            }

        })

    }

    override fun onBackPressed() {

        if (pager.currentItem != 0) {
            pager.arrowScroll(View.FOCUS_LEFT)
        } else
            super.onBackPressed()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun prepareDots(currentPageIndex: Int) {
        if (pageIndicator.childCount > 0) {
            pageIndicator.removeAllViews()
        }

        var dots = mutableListOf<ImageView>()

        for (i in 0..3) {
            dots.add(ImageView(this))

            if (i == pager.currentItem) {
                dots[i].setImageDrawable(getDrawable(R.drawable.active_dot))
            } else {
                dots[i].setImageDrawable(getDrawable(R.drawable.inactive_dot))
            }

            var layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            )

            layoutParams.setMargins(12, 0, 12, 0)

            pageIndicator.addView(dots[i], layoutParams)
        }
    }
}
