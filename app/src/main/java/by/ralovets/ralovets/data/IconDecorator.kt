package by.ralovets.ralovets.data

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class IconDecorator(val space: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.left = space;
        outRect.right = space;
        outRect.bottom = space
    }
}