package by.ralovets.ralovets.data

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.ralovets.ralovets.R

class IconGridAdapter(
        private val myDataset: MutableList<AppDetails>,
        private val context: Context,
        private val mOnLongClickGridListener: OnLongClickGridListener,
        private val mOnClickGridListener: OnClickGridListener,
        private val itemWidth: Int) :
        RecyclerView.Adapter<IconGridAdapter.IconGridItemViewHolder>() {

    class IconGridItemViewHolder(
            v: View,
            private val onLongClickGridListener: OnLongClickGridListener,
            private val onClickGridListener: OnClickGridListener) :
            RecyclerView.ViewHolder(v),
            View.OnLongClickListener,
            View.OnClickListener {

        val viewCell: View = v.findViewById(R.id.iconColored)
        val iconContainer: RelativeLayout = v.findViewById(R.id.iconContainer)
        val appName: TextView = v.findViewById(R.id.appName)

        init {
            itemView.setOnLongClickListener(this)
            itemView.setOnClickListener(this)
        }

        override fun onLongClick(v: View?): Boolean {
            onLongClickGridListener.onLongGridItemClick(adapterPosition)
            return true
        }

        override fun onClick(v: View?) {
            onClickGridListener.onGridItemClick(adapterPosition)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): IconGridItemViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.test_item, parent, false)
        return IconGridItemViewHolder(v, mOnLongClickGridListener, mOnClickGridListener)
    }

    override fun onBindViewHolder(holder: IconGridItemViewHolder, position: Int) {

        holder.viewCell.setBackgroundDrawable(myDataset[position].icon)
        holder.appName.text = myDataset[position].label
        var layoutParams = RelativeLayout.LayoutParams(itemWidth, itemWidth)
        holder.iconContainer.layoutParams = layoutParams
    }

    override fun getItemCount() = myDataset.size

    interface OnLongClickGridListener{
        fun onLongGridItemClick(position: Int)
    }

    interface OnClickGridListener{
        fun onGridItemClick(position: Int)
    }

}