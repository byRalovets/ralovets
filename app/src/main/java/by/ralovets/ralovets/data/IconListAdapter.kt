package by.ralovets.ralovets.data

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import by.ralovets.ralovets.R

class IconListAdapter(
        private val myDataset: MutableList<AppDetails>,
        private val context: Context,
        private val mOnClickListListener: IconListAdapter.OnClickListListener,
        private val mOnLongClickListListener: IconListAdapter.OnLongClickListListener) :
        RecyclerView.Adapter<IconListAdapter.IconListItemViewHolder>() {

    class IconListItemViewHolder(
            v: View,
            var onLongClickListListener: OnLongClickListListener,
            var onClickListListener: OnClickListListener) :
            RecyclerView.ViewHolder(v), View.OnLongClickListener, View.OnClickListener {

        val viewCell: View = v.findViewById(R.id.just_view)
        val appLabel: TextView = v.findViewById(R.id.textView8)
        val appName: TextView = v.findViewById(R.id.textView11)

        override fun onLongClick(v: View?): Boolean {
            onLongClickListListener.onLongListItemClick(adapterPosition)
            return true
        }

        override fun onClick(v: View?) {
            onClickListListener.onListItemClick(adapterPosition)
        }

        init {
            v.setOnLongClickListener(this)
            v.setOnClickListener(this)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): IconListItemViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_list_item, parent, false)
        return IconListItemViewHolder(v, mOnLongClickListListener, mOnClickListListener)
    }

    override fun onBindViewHolder(holder: IconListItemViewHolder, position: Int) {
        holder.viewCell.setBackgroundDrawable(myDataset[position].icon)
        holder.appLabel.text = myDataset[position].label
        holder.appName.text = myDataset[position].name
    }

    override fun getItemCount() = myDataset.size

    interface OnLongClickListListener{

        fun onLongListItemClick(position: Int)
    }

    interface OnClickListListener {

        fun onListItemClick(position: Int)
    }

}